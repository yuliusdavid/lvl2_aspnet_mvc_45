﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using System.Data.Entity;


namespace LVL2_ASPNet_MVC_45.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();
        public ActionResult index()
        {
            return View(db.Employees.ToList());
        }

        public ActionResult Details(int id)
        {
            return View(db.Employees.Where(x=> x.Id == id).FirstOrDefault());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]

        public ActionResult Create(Employee employee)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Employee emp = new Employee();

                    db.Employees.Add(emp);
                    db.SaveChanges();

                    if (emp.Id > 0)
                    {
                        /*int a = 0;
                        int total = 10 / a;*/
                        Salary sal = new Salary();
                        sal.Id_employee = emp.Id;
                        sal.Salary1 = emp.Salary;
                        db.Salaries.Add(sal);
                        db.SaveChanges();
                        ts.Commit();
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            Employee employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employee);
        }
        [HttpPost]

        public ActionResult Edit(Employee employee)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(employee).State = EntityState.Modified;
                    db.SaveChanges();
                    if(employee.Id > 0)
                    {
                        /*int a = 0;
                        int total = 10 / a;*/
                        Salary sal = db.Salaries.Where(x => x.Id_employee == employee.Id).FirstOrDefault();
                        sal.Id_employee = employee.Id;
                        db.Entry(sal).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            Employee employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employee);
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                    /*int a = 0;
                    int total = 10 / a;*/

                    Employee e = db.Employees.Where(x => x.Id == id).FirstOrDefault();
                    db.Employees.Remove(e);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                    
            }
            catch(Exception ex)
            {
                ViewData["data"] = ex.Message;
                return View();
            }
        }
    }
}